package main

import "bytes"

// Request data for Post
// swagger:parameters Post
type PostRequest struct {
	// in: formData
	//
	// swagger:file
	File *bytes.Buffer `json:"file"`
}

// Request data for Put
// swagger:parameters Put
type PutRequest struct {
	// in: formData
	//
	// swagger:file
	File *bytes.Buffer `json:"file"`
}

// Request parametr in url query
// swagger:parameters Put
type DeleteQueryArg struct {
	// Determines whether to delete the old project directory and files
	//
	// in: query
	Delete bool `json:"delete"`
}

// Request parametr in url path
// swagger:parameters Delete GetOne Post
type FilePathArg struct {
	// Path to the file
	//
	// in: path
	Path string `json:"path"`
}

// Request parametr in url path
// swagger:parameters GetOne ListAll Delete Put Post
type ProjectIdArg struct {
	// Id of the project
	//
	// in: path
	Id string `json:"id"`
}