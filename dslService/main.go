/*
Package main Finxact DSL service API.

	Schemes: 
		https
		http
	Host: HOST
	BasePath: /
	Version: v2.0.0
	Consumes:
	- application/json
	Produces:
	- application/json

swagger:meta
*/
package main

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
)

func main() {
	r := chi.NewRouter()
	r.Route("/test", func(r chi.Router) {
		r.Post("/post/{id}/{path}", Post)
		r.Put("/put/{id}", Put)
		r.Delete("/delete/{id}/{path}", Delete)
		r.Get("/listAll/{id}", ListAll)
		r.Get("/getOne/{id}/{path}", GetOne)
	})

	if err := http.ListenAndServe(fmt.Sprintf(":8080"), r); err != nil {
		fmt.Printf("HTTP serve returned error: %s", err)
	}
}