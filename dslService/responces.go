package main

// Default response for api v2
// swagger:response ApiV2Response
type ApiV2Response struct {
	// in: body
	Body struct {
		// The content of the requested file
		Data string
	}
}

// GetFileResponse returns file if request was successful
// swagger:response GetFileResponse
type GetFileResponse struct {
	// in: body
	Body struct {
		// The content of the requested file
		FileTest string
	}
}
