package main

import "net/http"

// swagger:route POST /test/post/{id}/{path} APIv2 Post
//
// Creates the file
//
// Creates the file from the multi-part file sent as well as any directories that are needed for the path
//
// Consumes:
//     - multipart/form-data
// Produces:
//     - application/json
// Responses:
//     default: ApiV2Response
func Post(w http.ResponseWriter, r *http.Request) {}

// swagger:route PUT /test/put/{id} APIv2 Put
//
// Update the project
//
// Decompresses the transferred files and updates the project directory with them.
// The upload path is the name of the compressed file.
// If flag "deleted=true" is passed in the URL query, the old directory will be deleted.
// Supported compression formats: .zip, .tar.gz, .tar.bz2
//
// Consumes:
//     - multipart/form-data
// Produces:
//     - application/json
// Responses:
//     default: ApiV2Response
func Put(w http.ResponseWriter, r *http.Request) {}

// swagger:route DELETE /test/delete/{id}/{path} APIv2 Delete
//
// Deletes file
//
// Deletes a file by the specified path
//
// Produces:
//     - application/json
// Responses:
//     default: ApiV2Response
func Delete(w http.ResponseWriter, r *http.Request) {}

// swagger:route GET /test/listAll/{id} APIv2 ListAll
//
// Returns all dirs and files
//
// Returns all dirs and files for the corresponding projectID
//
// Produces:
//     - application/json
// Responses:
//     default: ApiV2Response
func ListAll(w http.ResponseWriter, r *http.Request) {}

// swagger:route GET /test/getOne/{id}/{path} APIv2 GetOne
//
// Returns transpiled file
//
// Returns transpiled file (.go) by the specified path
//
// Produces:
//     - application/json
// Responses:
// 	   200: GetFileResponse
// 	   204: description: No content or empty file
//     default: ApiV2Response
func GetOne(w http.ResponseWriter, r *http.Request) {}
